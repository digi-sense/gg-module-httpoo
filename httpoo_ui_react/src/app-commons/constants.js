import axios from "axios";

const host = "http://" + window.location.hostname + ":9090/"

const constants = {
    APP_NAME: "HTTPoo sample GUI",
    APP_VERSION: "v1.0.0",

    FLD_USER: "httpoo-user",
    FLD_EMAIL: "httpoo-email",
    FLD_TOKEN_CONFIRM: "httpoo-token-confirm",
    FLD_TOKEN_ACCESS: "httpoo-token",
    FLD_TOKEN_REFRESH: "httpoo-token-refresh",

    HOST: host,

    API: createApi,

    RouteHome:"/home",
    RouteLogin:"/login",
    RouteRegister:"/register",
    RouteProfile:"/profile",

    EventLoginStart: "login-start",
    EventLoginSuccess: "login-success",
    EventLoginError: "login-error",
    EventLoginExit: "login-exit",
    EventRegisterSuccess:"register-success"
}

function createApi() {
    const token = localStorage["httpoo-token"];
    const headers = {};
    if (!!token) {
        headers["Authorization"] = "Bearer " + token;
    }
    return axios.create(
        {
            baseURL: host,
            responseType: 'json',
            headers: headers
        });
}

export default constants;