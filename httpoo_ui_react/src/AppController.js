import {EventEmitterSingleton} from "./libs/events/EventEmitter";
import constants from "./app-commons/constants";
import random from "./libs/random";

class AppController {

    constructor() {
        this.key = random.guid();
        console.debug("AppController initialized: ", this.key);
    }

    open() {
        EventEmitterSingleton.on(this, constants.EventLoginError, this.onEventLoginError);
        EventEmitterSingleton.on(this, constants.EventLoginSuccess, this.onEventLoginSuccess);
        EventEmitterSingleton.on(this, constants.EventLoginExit, this.onEventLoginExit);
        EventEmitterSingleton.on(this, constants.EventRegisterSuccess, this.onEventRegisterSuccess);
    }

    onEventLoginSuccess(user) {
        const auth = !!user ? user.auth : false;
        if (!!auth) {
            const accessToken = auth["access_token"]
            const refreshToken = auth["refresh_token"]
            localStorage[constants.FLD_USER] = JSON.stringify(user);
            localStorage[constants.FLD_TOKEN_ACCESS] = accessToken;
            localStorage[constants.FLD_TOKEN_REFRESH] = refreshToken;
            localStorage[constants.FLD_TOKEN_CONFIRM] = ""; // remove confirm token
            console.debug("AppController.onEventLoginSuccess", user, accessToken);
        }
    }

    onEventRegisterSuccess(response) {
        const confirmToken = !!response ? response["confirm_token"] : "";
        const email = !!response ? response["email"] : "";
        if (!!confirmToken) {
            localStorage[constants.FLD_TOKEN_CONFIRM] = confirmToken;
            localStorage[constants.FLD_EMAIL] = email;
        }
    }

    onEventLoginError(err) {
        console.debug("AppController.onEventLoginError", err);
    }

    onEventLoginExit() {
        localStorage[constants.FLD_TOKEN_ACCESS] = "";
        localStorage[constants.FLD_TOKEN_REFRESH] = "";
        localStorage[constants.FLD_TOKEN_CONFIRM] = "";
        localStorage[constants.FLD_USER] = "";
        localStorage[constants.FLD_EMAIL] = "";
    }


}

export default AppController;