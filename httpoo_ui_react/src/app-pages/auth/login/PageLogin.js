import {Component, Fragment} from "react";
import CompLogin from "../../../app-components/login/CompLogin";
import constants from "../../../app-commons/constants";
import {EventEmitterSingleton} from "../../../libs/events/EventEmitter";
import random from "../../../libs/random";
import {withRouter} from "react-router-dom";
import {Col, Row} from "antd";
import Title from "antd/es/typography/Title";

class PageLogin extends Component {

    history;
    state = {
        loading: false
    };

    constructor(props) {
        super(props);
        this.key = random.id();
        this.history = props.history;
    }

    componentDidMount() {
        EventEmitterSingleton.on(this, constants.EventLoginStart, () => {
            this.setState({loading: true});
        });
        EventEmitterSingleton.on(this, constants.EventLoginSuccess, () => {
            this.history.push("/home");
        });
        EventEmitterSingleton.on(this, constants.EventLoginError, (err) => {
            this.setState({loading: false});
        });
    }

    componentWillUnmount() {
        EventEmitterSingleton.off(this);
    }

    render() {
        const loading = !!this.state.loading;
        return (
            <Fragment>
                <div style={{textAlign:"center"}}>
                    <Title>LOGIN PAGE</Title>
                    <Row justify="center">
                        <Col span={12}>
                            <CompLogin loading={loading}/>
                        </Col>
                    </Row>
                </div>

            </Fragment>

        );
    }
}

export default withRouter(PageLogin)