import Events, {Listener} from "./Events";

/**
 * Simple emitter without a context
 */
class Emitter {

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------

    private _events: Events;

    // ------------------------------------------------------------------------
    //                      c o ns t r u c t o r
    // ------------------------------------------------------------------------

    constructor() {
        this._events = new Events();
    }

    // ------------------------------------------------------------------------
    //                      p u b l i c
    // ------------------------------------------------------------------------

    public on(event_name: string, listener: Listener): void {
        // ly events
        this._events.removeListener(event_name, listener);
        this._events.on(event_name, listener);
    }

    public off(event_names?: string | string[], listener?: Listener): void {
        this._events.off(event_names, listener);
    }

    public emit(eventName: string, ...args: any[]): boolean {
        return this._events.emit(eventName, ...args);
    }

    // ------------------------------------------------------------------------
    //                      S I N G L E T O N
    // ------------------------------------------------------------------------

    private static __instance: Emitter;

    public static instance(): Emitter {
        if (null == Emitter.__instance) {
            Emitter.__instance = new Emitter();
        }
        return Emitter.__instance;
    }

}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

const EmitterSingleton: Emitter = Emitter.instance();
export {EmitterSingleton, Emitter};