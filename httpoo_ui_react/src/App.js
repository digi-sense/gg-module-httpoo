import {HashRouter, Redirect, Route, Switch} from "react-router-dom";
import PageLogin from "./app-pages/auth/login/PageLogin";
import PageRegister from "./app-pages/auth/register/PageRegister";
import PageDashboard from "./app-pages/dashboard/PageDashboard";
import {PrivateRoute} from "./app-routing/PrivateRoute";
import './App.css';
import 'antd/dist/antd.css';
import constants from "./app-commons/constants";
import PageProfile from "./app-pages/auth/profile/PageProfile";

function App() {
    return (
        <HashRouter>
            <Switch>
                <Route exact path="/">
                    <Redirect to={constants.RouteHome}/>
                </Route>
                <Route exact path={[constants.RouteLogin]}>
                    <PageLogin/>
                </Route>
                <Route exact path={[constants.RouteRegister]}>
                    <PageRegister/>
                </Route>
                
                <PrivateRoute exact component={PageDashboard} path={constants.RouteHome}/>
                <PrivateRoute exact component={PageProfile} path={constants.RouteProfile}/>
            </Switch>
        </HashRouter>
    );
}

export default App;
