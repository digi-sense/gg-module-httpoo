import {Component, Fragment} from "react";
import {Button, Checkbox, Form, Input, message, Skeleton} from 'antd';
import {EventEmitterSingleton} from "../../libs/events/EventEmitter";
import constants from "../../app-commons/constants";
import services from "../../app-commons/services";
import fmt from "../../libs/fmt";

class CompRegister extends Component {

    state = {error: ""};

    constructor(props) {
        super(props);
        this.fields = {
            username: null,
            password: null,
            password2: null,
            firstname: null,
            lastname: null,
            privacy_policy_consent: null,
        }
    }

    setRefUsername(element) {
        this.fields.username = element;
        if (!!this.fields.username) {
            this.fields.username.value = "";
        }
    }

    setRefPassword(element) {
        this.fields.password = element;
        if (!!this.fields.password) {
            this.fields.password.value = "";
        }
    }

    setRefPassword2(element) {
        this.fields.password2 = element;
        if (!!this.fields.password2) {
            this.fields.password2.value = "";
        }
    }

    setRefFirstname(element) {
        this.fields.firstname = element;
        if (!!this.fields.firstname) {
            this.fields.firstname.value = "";
        }
    }

    setRefLastname(element) {
        this.fields.lastname = element;
        if (!!this.fields.lastname) {
            this.fields.lastname.value = "";
        }
    }

    setRefPrivacy(element) {
        this.fields.privacy_policy_consent = element;
        if (!!this.fields.privacy_policy_consent) {
            this.fields.privacy_policy_consent.value = true;
        }
    }

    // JUST FOR TESTING A FORBIDDEN ACCESS
    doProtectedQuery() {
        services.user((err, user) => {
            if (!!err) {
                message.error(fmt.template("Error: ({{code}}) {{message}}", err));
            } else if (!!user) {
                message.info("Welcome, " + user.firstname);
            }
        })
    }

    onSubmit() {
        if (!!this) {
            if (!!this.fields.username && !!this.fields.password) {
                const username = this.fields.username.state.value;
                const password = this.fields.password.state.value;
                const firstname = this.fields.firstname.state.value;
                const lastname = this.fields.lastname.state.value;
                const privacy_policy_consent = this.fields.privacy_policy_consent.props.checked;
                console.debug("onSubmit", username, password, firstname, lastname, privacy_policy_consent);
                if (!!username && !!password && !!privacy_policy_consent) {
                    // emit start
                    EventEmitterSingleton.emit(constants.EventLoginStart);
                    // invoke service
                    services.register(username, password, firstname, lastname, privacy_policy_consent, (error, response) => {
                        if (!!error) {
                            message.error(fmt.template("Error: ({{code}}) {{message}}", error));
                            // emit error
                            EventEmitterSingleton.emit(constants.EventLoginError, error);
                        } else if (!!response) {
                            // got a response containing user and tokens.
                            EventEmitterSingleton.emit(constants.EventRegisterSuccess, response);
                        }
                    });
                } else {
                    // missing username or password
                    message.error("Missing username, password or privacy consent.");
                }
            } else {
                // missing binding
                message.error("System Error.");
            }
        }
    }

    render() {
        const loading = !!this.props.loading;

        return (
            <Fragment>
                {loading
                    ? <Skeleton active/>
                    : <Form
                        name="basic"
                        labelCol={{span: 8}}
                        wrapperCol={{span: 16}}
                        initialValues={{remember: true}}
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[{required: true, message: 'Please input your username!'}]}
                        >
                            <Input ref={this.setRefUsername.bind(this)}/>
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[{required: true, message: 'Please input your password!'}]}
                        >
                            <Input.Password ref={this.setRefPassword.bind(this)}/>
                        </Form.Item>

                        <Form.Item
                            label="Repeat Password"
                            name="password2"
                            rules={[{required: true, message: 'Please repeat your password!'}]}
                        >
                            <Input.Password ref={this.setRefPassword2.bind(this)}/>
                        </Form.Item>

                        <Form.Item
                            label="First Name"
                            name="firstname"
                            rules={[{required: true, message: 'First Name'}]}
                        >
                            <Input ref={this.setRefFirstname.bind(this)}/>
                        </Form.Item>

                        <Form.Item
                            label="First Name"
                            name="lastname"
                            rules={[{required: true, message: 'Last Name'}]}
                        >
                            <Input ref={this.setRefLastname.bind(this)}/>
                        </Form.Item>

                        <Form.Item name="privacy_policy_consent" valuePropName="checked" wrapperCol={{offset: 8, span: 16}}>
                            <Checkbox ref={this.setRefPrivacy.bind(this)}>Privacy Consent</Checkbox>
                        </Form.Item>

                        <Form.Item wrapperCol={{offset: 8, span: 16}}>
                            <Button type="primary" onClick={this.onSubmit.bind(this)}>
                                Register
                            </Button>
                        </Form.Item>
                    </Form>
                }

            </Fragment>
        );
    }
}

export default CompRegister