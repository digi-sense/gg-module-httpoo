# HTTPoo #

Simple webserver with Authentication API written in Go.

![icon](icon_128.png)

## Introduction ##

HTTPoo is a webserver written in Go using [Fiber](https://github.com/gofiber/fiber)
with Authentication API implementation using JWT, full of email/sms messages
(registration welcome, password forget, etc...) with users.

HTTPoo is fully customizable, templates are customizable and you can opt to use HTTPoo as a library in your Go code or
as a deployable webserver.

## How to Use Binary ##

HTTPoo is released with already compiled binary code for:

 - Windows 32
 - Windows 64
 - Mac OS
 - Linux
 - Raspberry

Binaries are deployed in [_build](./_build) directory.

## How to Use the Go Module ##

To use just call:

```
go get bitbucket.org/digi-sense/gg-module-httpoo@latest
```

SAMPLE CODE

```go
package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_commons"
	"bitbucket.org/lygo/lygo_ext_httpoo/httpoo/httpoo_types"
	"github.com/gofiber/fiber/v2"
	"log"
	"testing"
	"time"
)

func TestCustomHandler(t *testing.T) {
	// set working path
	lygo_paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(lygo_paths.Absolute("./"))

	// create server
	server, err := httpoo.LaunchHTTPoo("debug", "stop")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// add custom API handler
	server.Handle("get", "custom/endpoint", nil, customHandler)
	err = server.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// get server URL
	url := server.LocalUrl()
	log.Println("STARTED:", url)

	// open browser
	go func() {
		// OPEN DEFAULT BROWSER NAVIGATING http://localhost:9090/custom/endpoint
		_ = lygo_exec.Open(url + "custom/endpoint")
	}()

	// wait 3 seconds and exit
	go func() {
		time.Sleep(3 * time.Second)
		log.Println("STOPPING...")
		_ = server.Stop()
	}()

	// wait for exit
	server.Join()
	log.Println("BYE BYE!")
}


// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func customHandler(ctx *fiber.Ctx, endpoint *httpoo_types.EndPoint) error {
	log.Println("GET: " + ctx.OriginalURL())
	return httpoo.WriteHtml(ctx, "<body><strong>DONE!</strong></body>")
}
```

## Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.5
git push origin v0.1.5
```

```
go get bitbucket.org/digi-sense/gg-module-httpoo@latest
```

--------------

## CHANGELOG ##

**[v. 0.1.5]**

- Refactoring

**[v. 0.1.4]**

- Minor improvements

**[v. 0.1.3]**

- Added method to securely update user data

**[v. 0.1.2]**

- Minor improvements

**[v. 0.1.1]**

- Added support for AQL/SQL commands

**[v. 0.0.9]**

- Update [React UI sample](./httpoo_ui_react): 
  - Refactoring to handle _token_expired_ error

**[v. 0.0.8]**

- Added [React UI sample](./httpoo_ui_react)

**[v. 0.0.7]**

- Sources refactoring

**[v. 0.0.2]**

- UPDATED BINARIES
  
**[v. 0.0.1]**

- Authentication API
- Bash file support: You can create custom API invoking bash file on file system and getting the console output as
  response.

--------------

## License Disclaimer 

HTTPoo depends on some LyGo Modules that have different license for commercial projects, but HTTPoo is
an exception and you are authorized to use LyGo Modules under MIT license if your projects include HTTPoo.

So, all LyGo Modules used in HTTPoo projects are to be considered under MIT license and not Commercial or AGPL License.

Enjoy HTTPoo.

## LICENSE (MIT) 

![license](mit.png)

Copyright 2021 Gian Angelo Geminiani

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.




