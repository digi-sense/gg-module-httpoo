module bitbucket.org/digi-sense/gg-module-httpoo

go 1.16

require (
	bitbucket.org/digi-sense/gg-core v0.1.18
	bitbucket.org/digi-sense/gg-core-x v0.1.18
	github.com/cbroglie/mustache v1.3.0
	github.com/gofiber/fiber/v2 v2.20.0
)
