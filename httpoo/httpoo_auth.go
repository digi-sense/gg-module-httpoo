package httpoo

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0"
	"bitbucket.org/digi-sense/gg-core/gg_ticker"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_ctrl"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_types"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"net/url"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	const
// ----------------------------------------------------------------------------------------------------------------------

const (
	channelEMAIL = "email"
	channelSMS   = "sms"

	tplVerify        = "verify"
	tplWelcome       = "welcome"
	tplError         = "error"                   // html
	tplVerified      = "verified"                // html
	tplReset         = "password-reset"          // html
	tplResetImported = "password-reset-imported" // html
)

var (
	UserAlreadyRegisteredError = errors.New("user_already_registered") // 401
	UserEmailError             = errors.New("user_email_error")        // 500
	UserNotFoundError          = errors.New("invalid_credentials")     // 401
	UserPermissionDeniedError  = errors.New("permission_denied")       // 403
	UserAuthDataError          = errors.New("user_auth_data_error")    // 401
	UserNotConfirmedError      = errors.New("user_not_confirmed")      // 401
	UserDataError              = errors.New("user_data_error")         // 500
	MissingParamsError         = errors.New("missing_params")          // 500
	MismatchParamsError        = errors.New("mismatch_params_error")   // 500
	ChannelNotFoundError       = errors.New("channel_not_found")       // 500
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserverAuthentication struct {
	enabled      bool
	mode         string
	dirWork      string
	dirWebserver string
	dirTemp      string
	logger       *httpoo_commons.Logger
	settings     *httpoo_types.WebServerSettingsAuthentication
	security     *httpoo_types.WebServerSettingsAuthenticationSecurity
	webserver    *httpoo_ctrl.Webserver
	routing      *httpoo_types.WebServerSettingsAuthenticationRouting
	auth0        *gg_auth0.Auth0
	database     *httpoo_ctrl.AppWebserverAuthenticationDatabase
	postman      *httpoo_ctrl.AppWebserverAuthenticationPostman
	ticker       *gg_ticker.Ticker
}

func NewAppWebserverAuthentication(mode string, dirWork, dirWebserver string, l *httpoo_commons.Logger,
	s *httpoo_types.WebServerSettingsAuthentication, webserver *httpoo_ctrl.Webserver,
	secureManager *httpoo_ctrl.AppSecure) *AppWebserverAuthentication {
	// creates new instance
	instance := new(AppWebserverAuthentication)
	instance.enabled = false
	instance.webserver = webserver
	instance.mode = mode
	instance.dirWork = dirWork
	instance.dirWebserver = dirWebserver
	instance.dirTemp = gg.Paths.Concat(dirWebserver, "tmp")
	instance.logger = l
	instance.settings = s
	instance.security = s.Security
	if nil == instance.security {
		instance.security = new(httpoo_types.WebServerSettingsAuthenticationSecurity)
	}
	if nil == instance.security.Authorization {
		instance.security.Authorization = new(httpoo_types.Authorization)
	}
	if nil != secureManager {
		instance.auth0 = secureManager.Auth0()
		if nil != secureManager.Settings() {
			instance.database = httpoo_ctrl.NewAppWebserverAuthenticationDatabase(secureManager.Settings().AuthStorage,
				instance.settings.DbUsersCollection)
		}
	}

	instance.init()

	if !instance.enabled {
		instance.logger.Info("BUILTIN AUTHENTICATION IS NOT ENABLED.")
	} else {
		// set custom duration
		instance.auth0.AccessTokenDuration = 30 * time.Minute
		instance.auth0.AccessCacheDuration = 24 * time.Hour
		instance.auth0.RefreshTokenDuration = instance.auth0.AccessCacheDuration
		instance.auth0.PasswordDurationDays = instance.settings.PasswordExpireInDays
		if instance.settings.AccountNotConfirmedExpireInDays > 0 {
			instance.auth0.ConfirmTokenDuration = time.Duration(instance.settings.AccountNotConfirmedExpireInDays) * (time.Hour * 24)
		}
		if instance.settings.AccessTokenExpireInSeconds > 0 {
			instance.auth0.AccessCacheDuration = time.Duration(instance.settings.AccessTokenExpireInSeconds) * time.Second
			instance.auth0.AccessTokenDuration = time.Duration(instance.settings.AccessTokenExpireInSeconds) * time.Second
		}

		// internal ticker to check accounts
		instance.ticker = gg_ticker.NewTicker(time.Hour*1, instance.onAuthTick)
		instance.ticker.Start()
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthentication) Enabled() bool {
	return instance.enabled
}

func (instance *AppWebserverAuthentication) Start() ([]string, []error) {
	if nil != instance.postman {
		return instance.postman.CheckTemplates()
	}
	return nil, nil
}

func (instance *AppWebserverAuthentication) Stop() {
	if nil != instance {
		if nil != instance.ticker {
			instance.ticker.Stop()
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthentication) init() {
	if nil != instance && nil != instance.settings && instance.settings.Enabled &&
		nil != instance.webserver && instance.webserver.IsEnabled() && nil != instance.auth0 {

		// add authentication endpoints
		routing := getRouting(instance.settings.Routing)
		if nil != routing {
			instance.routing = routing
			instance.webserver.Handle(routing.AuthSignIn.Method, routing.AuthSignIn.Endpoint, instance.onAuthSignIn)
			instance.webserver.Handle(routing.AuthSignUp.Method, routing.AuthSignUp.Endpoint, instance.onAuthSignUp)
			instance.webserver.Handle(routing.AuthUserGet.Method, routing.AuthUserGet.Endpoint, instance.onAuthUserGet)
			instance.webserver.Handle(routing.AuthUserPost.Method, routing.AuthUserPost.Endpoint, instance.onAuthUserPost)
			instance.webserver.Handle(routing.AuthVerify.Method, routing.AuthVerify.Endpoint, instance.onAuthVerify)
			instance.webserver.Handle(routing.AuthForgotPassword.Method, routing.AuthForgotPassword.Endpoint, instance.onAuthForgotPassword)
			instance.webserver.Handle(routing.AuthChangePassword.Method, routing.AuthChangePassword.Endpoint, instance.onAuthChangePassword)
			instance.webserver.Handle(routing.AuthResetPassword.Method, routing.AuthResetPassword.Endpoint, instance.onAuthResetPassword)
			instance.webserver.Handle(routing.AuthRemove.Method, routing.AuthRemove.Endpoint, instance.onAuthRemove)
			instance.webserver.Handle(routing.AuthImport.Method, routing.AuthImport.Endpoint, instance.onAuthImport)
			instance.webserver.Handle(routing.AuthResendActivationEmail.Method, routing.AuthResendActivationEmail.Endpoint, instance.onAuthResendActivationEmail)
			instance.webserver.Handle(routing.AuthValidateToken.Method, routing.AuthValidateToken.Endpoint, instance.onAuthValidateToken)
			instance.webserver.Handle(routing.AuthRefreshToken.Method, routing.AuthRefreshToken.Endpoint, instance.onAuthRefreshToken)
			instance.webserver.Handle(routing.AuthGrantDelegation.Method, routing.AuthGrantDelegation.Endpoint, instance.onAuthGrantDelegation)
			instance.webserver.Handle(routing.AuthRevokeDelegation.Method, routing.AuthRevokeDelegation.Endpoint, instance.onAuthRevokeDelegation)

			// postman
			if nil == instance.settings.Postman.Payload {
				instance.settings.Postman.Payload = map[string]interface{}{}
			}
			instance.settings.Postman.Payload["app-base-url"] = instance.settings.AppBaseUrl
			instance.settings.Postman.Payload["api-base-url"] = instance.settings.ApiBaseUrl
			instance.postman = httpoo_ctrl.NewAppWebserverAuthenticationPostman(instance.dirWork, instance.settings.DirTemplates,
				instance.settings.Postman)

			// finally set enabled flag
			instance.enabled = true
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	h a n d l e r s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthentication) onAuthSignUp(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthSignUp.ParamsRequired
		names := instance.routing.AuthSignUp.Params
		if len(names) == 0 {
			names = required
		}
		_, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		params := Params(ctx, true, names...)
		email := gg.Reflect.GetString(params, names[0])
		password := gg.Reflect.GetString(params, names[1])

		// does user exists
		exists, err := instance.database.UserEmailExists(names[0], email)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		if exists {
			// user already registered
			return WriteResponse(ctx, nil, UserAlreadyRegisteredError)
		}

		// register user
		resp := instance.auth0.AuthSignUp(email, password, nil)
		if len(resp.Error) > 0 {
			return WriteResponse(ctx, nil, errors.New(resp.Error))
		}

		// upsert user into database
		user, _ := gg.JSON.StringToMap(gg.JSON.Stringify(params))
		delete(user, names[1]) // remove password
		_, err = instance.database.CreateNewUser(resp, user)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}

		// send confirmation message
		addresses := getAddresses(params, email)
		channels := getChannels(params)
		if len(addresses) != len(channels) {
			return WriteResponse(ctx, nil, MismatchParamsError)
		}
		err = instance.sendNotificationVerify(email, resp.ConfirmToken, addresses, channels)
		if nil != err {
			return WriteResponse(ctx, nil, gg.Errors.Prefix(UserEmailError, err.Error()))
		}

		// ready to send back response
		responseData := map[string]interface{}{
			// "auth_id":       lygo_reflect.GetString(newUser, "auth_id"),
			"confirm_token": resp.ConfirmToken,
			"email":         email,
		}
		return WriteResponse(ctx, responseData, nil)
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthUserGet(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		if AuthenticateRequest(ctx, instance.auth0, instance.security.Authorization, true) {
			var user map[string]interface{}
			token := getAuthenticationToken(ctx)
			parsed, err := instance.auth0.TokenParse(token)
			if nil == err {
				authId := gg.Reflect.GetString(parsed, "user_id")
				user, err = instance.database.GetUser("auth_id", authId)
			}
			return WriteResponse(ctx, user, err)
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthUserPost(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		if AuthenticateRequest(ctx, instance.auth0, instance.security.Authorization, true) {
			user := BodyMap(ctx, true)
			response, err := instance.database.UpdateUser(user, false)
			return WriteResponse(ctx, response, err)
		}
	}
	return nil
}

// onAuthVerify Confirm The account validating email
func (instance *AppWebserverAuthentication) onAuthVerify(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthVerify.ParamsRequired
		params, err := AssertParams(ctx, required)
		if nil != err {
			return instance.writeHtmlError(ctx, err)
		}
		if len(required) > 0 {
			token := gg.Reflect.GetString(params, required[0])
			resp := instance.auth0.AuthConfirm(token)
			if len(resp.Error) > 0 {
				return instance.writeHtmlError(ctx, errors.New(resp.Error))
			}
			authId := resp.ItemId
			user, err := instance.database.ConfirmUser(authId)
			if nil != err {
				return instance.writeHtmlError(ctx, err)
			}
			email := gg.Reflect.GetString(user, "email")
			if len(email) > 0 {
				// send welcome
				go instance.postman.SendEmailTemplate(tplWelcome, email, user)
			}
			html, err := instance.postman.GetHtmlTemplate(tplVerified, user)
			if nil != err {
				return instance.writeHtmlError(ctx, err)
			}
			// send response
			return WriteHtml(ctx, html)
		} else {
			return WriteResponse(ctx, nil, MissingParamsError)
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthSignIn(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthSignIn.ParamsRequired
		params, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		if len(required) > 1 {
			username := gg.Reflect.GetString(params, required[0])
			password := gg.Reflect.GetString(params, required[1])
			resp, err := instance.authenticate(username, password, required[0])
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			authId := gg.Reflect.GetString(resp, "auth_id")
			if len(authId) == 0 {
				return WriteResponse(ctx, nil, UserAuthDataError)
			}
			user, err := instance.database.GetUser("auth_id", authId)

			// add auth info to logged-in user
			user["auth"] = resp

			return WriteResponse(ctx, user, err)
		} else {
			return WriteResponse(ctx, nil, MissingParamsError)
		}
	}
	return nil
}

// onAuthForgotPassword send email with link to change password. Required: "email"
func (instance *AppWebserverAuthentication) onAuthForgotPassword(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthForgotPassword.ParamsRequired
		params, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}

		if len(required) > 0 {
			email := gg.Reflect.GetString(params, required[0])
			user, err := instance.database.GetUser(required[0], email)
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			if nil == user {
				return WriteResponse(ctx, nil, UserNotFoundError)
			}

			authId := gg.Reflect.GetString(user, "auth_id")
			if len(authId) == 0 {
				return WriteResponse(ctx, nil, UserDataError)
			}

			// send confirmation message with HTML page linked
			addresses := getAddresses(params, email)
			channels := getChannels(params)
			if len(addresses) != len(channels) {
				return WriteResponse(ctx, nil, MismatchParamsError)
			}
			err = instance.sendNotificationResetPassword(email, authId, addresses, channels)
			if nil != err {
				return WriteResponse(ctx, nil, gg.Errors.Prefix(UserEmailError, err.Error()))
			}

			return WriteResponse(ctx, true, nil)
		} else {
			return WriteResponse(ctx, nil, MissingParamsError)
		}
	}
	return nil
}

// just render HTML page to submit the password
func (instance *AppWebserverAuthentication) onAuthChangePassword(ctx *fiber.Ctx) error {
	required := instance.routing.AuthChangePassword.ParamsRequired
	params, err := AssertParams(ctx, required)
	if nil != err {
		return WriteResponse(ctx, nil, err)
	}

	// add link for submission
	link := "." + instance.routing.AuthResetPassword.Endpoint
	params["link"] = link
	params["app-token"] = instance.settings.Security.AppToken

	html, err := instance.postman.GetHtmlTemplate(tplReset, params) // pass token=auth_id
	if nil != err {
		return instance.writeHtmlError(ctx, err)
	}
	return WriteHtml(ctx, html)
}

// [SET NEW PASSWORD] receive request directly from UI, that submit a new password
func (instance *AppWebserverAuthentication) onAuthResetPassword(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		names := instance.routing.AuthResetPassword.Params // all parameters
		required := instance.routing.AuthResetPassword.ParamsRequired
		if len(names) < len(required) {
			n := append([]string{}, required...)
			n = append(n, names...)
			names = n
		}
		if len(names) != len(required)+1 {
			return WriteResponse(ctx, nil, MismatchParamsError)
		}
		params, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		if len(required) > 0 {
			authId := gg.Reflect.GetString(params, required[0])
			newPassword := gg.Reflect.GetString(params, required[1])
			if len(authId) > 0 && len(newPassword) > 0 {
				fldEmail := gg.Arrays.GetAt(names, 2, "email").(string)
				user, err := instance.resetPassword(required[0], fldEmail, authId, newPassword, true)
				if nil != err {
					return WriteResponse(ctx, nil, err)
				}

				return WriteResponse(ctx, map[string]interface{}{
					"user":     user,
					"redirect": instance.settings.AppBaseUrl,
				}, nil)

			} else {
				return WriteResponse(ctx, nil, MissingParamsError)
			}
		} else {
			return WriteResponse(ctx, nil, MissingParamsError)
		}
	}
	return nil
}

// onAuthRemove remove user account
func (instance *AppWebserverAuthentication) onAuthRemove(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		if AuthenticateRequest(ctx, instance.auth0, instance.security.Authorization, true) {
			required := instance.routing.AuthRemove.ParamsRequired
			params, err := AssertParams(ctx, required)
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			if len(params) > 0 {
				key := gg.Reflect.GetString(params, required[0])
				user, err := instance.removeUser(key)
				if nil != err {
					return WriteResponse(ctx, nil, err)
				}
				return WriteResponse(ctx, user, nil)
			} else {
				return WriteResponse(ctx, nil, MissingParamsError)
			}
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthResendActivationEmail(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthResendActivationEmail.ParamsRequired
		names := instance.routing.AuthResendActivationEmail.Params
		if len(names) == 0 {
			names = required
		}
		_, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		params := Params(ctx, true, names...)
		email := gg.Reflect.GetString(params, names[0])

		addresses := getAddresses(params, email)
		channels := getChannels(params)
		if len(addresses) != len(channels) {
			return WriteResponse(ctx, nil, MismatchParamsError)
		}

		// get user using first parameter that should be email/username
		user, err := instance.database.GetUser(names[0], email)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		if nil == user {
			return WriteResponse(ctx, nil, UserNotFoundError)
		}
		confirmToken := gg.Reflect.GetString(user, "confirm_token")
		if len(confirmToken) == 0 {
			return WriteResponse(ctx, nil, UserDataError)
		}

		// send confirmation message
		err = instance.sendNotificationVerify(email, confirmToken, addresses, channels)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		// ready to send back response
		responseData := map[string]interface{}{
			// "auth_id":       lygo_reflect.GetString(newUser, "auth_id"),
			"confirm_token": confirmToken,
			"email":         email,
		}
		return WriteResponse(ctx, responseData, nil)

	}
	return nil
}

// import csv or json file
func (instance *AppWebserverAuthentication) onAuthImport(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		if AuthenticateRequest(ctx, instance.auth0, instance.security.Authorization, true) {
			required := instance.routing.AuthImport.ParamsRequired
			params, err := AssertParams(ctx, required)
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			if len(params) > 0 {
				fldEmail := gg.Reflect.GetString(params, required[0])

				// upload files
				files, err := Upload(ctx, instance.dirTemp, 0)
				defer removeFiles(files, instance.dirTemp)
				if nil != err {
					return WriteResponse(ctx, nil, err)
				}

				// start import
				payloads := make([]map[string]interface{}, 0)
				for _, name := range files {
					filename := gg.Paths.Concat(instance.dirTemp, name)
					items, err := instance.database.ParseUserFromFile(filename)
					if nil != err {
						return WriteResponse(ctx, nil, err)
					}
					payloads = append(payloads, items...)
				}

				// create auth and register users
				users := make([]map[string]interface{}, 0)
				for _, payload := range payloads {
					email := gg.Reflect.GetString(payload, fldEmail)
					password := gg.Rnd.Uuid()
					exists, _ := instance.database.UserEmailExists(fldEmail, email)
					if exists {
						continue
					}
					// register user
					resp := instance.auth0.AuthSignUp(email, password, nil)
					if len(resp.Error) > 0 {
						continue
					}
					// create user into database
					user, err := instance.database.CreateNewUser(resp, payload)
					if nil != err {
						continue
					}
					users = append(users, user)
				}

				// send message to reset password at all users
				for _, user := range users {
					email := gg.Reflect.GetString(user, fldEmail)
					authId := gg.Reflect.GetString(user, "auth_id")
					addresses := getAddresses(params, email)
					channels := getChannels(params)

					go instance.sendNotificationResetPasswordImported(email, authId, addresses, channels)
				}

				return WriteResponse(ctx, len(users), err)
			}
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthValidateToken(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthVerify.ParamsRequired
		params, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		if len(required) > 0 {
			token := gg.Reflect.GetString(params, required[0])
			authId, expDate, err := instance.parseTokenAndGetInfo(token)
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			response := map[string]interface{}{
				"auth_id":        authId,
				"exp_timestamp":  expDate.Unix(),
				"exp_date":       expDate.String(),
				"exp_in_minutes": time.Now().Sub(expDate).Minutes() * -1,
			}
			return WriteResponse(ctx, response, nil)
		} else {
			return WriteResponse(ctx, nil, MissingParamsError)
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthRefreshToken(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		required := instance.routing.AuthVerify.ParamsRequired
		params, err := AssertParams(ctx, required)
		if nil != err {
			return WriteResponse(ctx, nil, err)
		}
		if len(required) > 0 {
			token := gg.Reflect.GetString(params, required[0])

			autResp := instance.auth0.TokenRefresh(token)
			if len(autResp.Error) > 0 {
				return WriteResponse(ctx, nil, httpoo_commons.RefreshTokenExpiredError)
			}
			return WriteResponse(ctx, map[string]interface{}{
				"access_token": autResp.AccessToken,
			}, nil)
		} else {
			return WriteResponse(ctx, nil, MissingParamsError)
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthGrantDelegation(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		if AuthenticateRequest(ctx, instance.auth0, instance.security.Authorization, true) {
			required := instance.routing.AuthGrantDelegation.ParamsRequired
			params, err := AssertParams(ctx, required)
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			if len(params) > 0 {
				// TODO: implement

			}
		}
	}
	return nil
}

func (instance *AppWebserverAuthentication) onAuthRevokeDelegation(ctx *fiber.Ctx) error {
	if validate(ctx, instance.security.AppToken) {
		if AuthenticateRequest(ctx, instance.auth0, instance.security.Authorization, true) {
			required := instance.routing.AuthRevokeDelegation.ParamsRequired
			params, err := AssertParams(ctx, required)
			if nil != err {
				return WriteResponse(ctx, nil, err)
			}
			if len(params) > 0 {
				// TODO: implement

			}
		}
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	u t i l s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthentication) resetPassword(fldAuthId, fldEmail, authId, newPassword string, changedByUser bool) (map[string]interface{}, error) {
	// get user
	user, err := instance.database.GetUser(fldAuthId, authId)
	if nil != err {
		return nil, err
	}
	email := gg.Reflect.GetString(user, fldEmail)
	if len(email) == 0 {
		// missing username
		return nil, UserDataError
	}

	// ready to change login
	authResp := instance.auth0.AuthChangeLogin(authId, email, newPassword)
	if len(authResp.Error) > 0 {
		return nil, errors.New(authResp.Error)
	}

	// update user
	user["auth_id"] = authResp.ItemId
	_, err = instance.database.UpdateUser(user, true)
	return user, err
}

func (instance *AppWebserverAuthentication) removeUser(key string) (map[string]interface{}, error) {
	user, err := instance.database.RemoveUser(key)
	if nil != err {
		return nil, err
	}
	authId := gg.Reflect.GetString(user, "auth_id")
	return user, instance.auth0.AuthRemoveByUserId(authId)
}

func (instance *AppWebserverAuthentication) parseTokenAndGetInfo(token string) (string, time.Time, error) {
	// parse token and get authId
	data, err := instance.auth0.TokenParse(token)
	if nil != err {
		return "", time.Now(), err
	}
	authId := gg.Reflect.GetString(data, "user_id")
	exp := gg.Reflect.GetInt(data, "exp")
	if len(authId) == 0 {
		return "", time.Now(), UserAuthDataError
	}
	expDate := time.Unix(int64(exp), 0)
	return authId, expDate, nil
}

func (instance *AppWebserverAuthentication) sendNotificationResetPasswordImported(email, authId string, container interface{}, channels []string) error {
	// send confirmation message
	payload := map[string]interface{}{
		"email": email,
		"token": authId,
		"link":  getLink(instance.routing.AuthChangePassword.Endpoint, authId), // HTML PAGE
	}
	return instance.sendNotification(tplResetImported, container, channels, payload)
}

func (instance *AppWebserverAuthentication) sendNotificationResetPassword(email, authId string, container interface{}, channels []string) error {
	// send confirmation message
	payload := map[string]interface{}{
		"email": email,
		"token": authId,
		"link":  getLink(instance.routing.AuthChangePassword.Endpoint, authId), // HTML PAGE
	}
	return instance.sendNotification(tplReset, container, channels, payload)
}

func (instance *AppWebserverAuthentication) sendNotificationVerify(email, confirmToken string, container interface{}, channels []string) error {
	// send confirmation message
	payload := map[string]interface{}{
		"email": email,
		"token": confirmToken,
		"link":  getLink(instance.routing.AuthVerify.Endpoint, confirmToken),
	}
	return instance.sendNotification(tplVerify, container, channels, payload)
}

func (instance *AppWebserverAuthentication) sendNotification(templateName string, container interface{}, channels []string, payloads ...map[string]interface{}) (err error) {
	addresses := gg.Convert.ToArrayOfString(container)
	for i, address := range addresses {
		channel := gg.Convert.ToString(gg.Arrays.GetAt(channels, i, ""))
		switch channel {
		case channelEMAIL:
			err = instance.postman.SendEmailTemplate(templateName, gg.Convert.ToString(address), payloads...)
		case channelSMS:
			_, err = instance.postman.SendSmsTemplate(templateName, gg.Convert.ToString(address), payloads...)
		default:
			err = gg.Errors.Prefix(ChannelNotFoundError, channel)
		}
		if nil != err {
			return
		}
	}
	return
}

func (instance *AppWebserverAuthentication) authenticate(username, password, field string) (map[string]interface{}, error) {
	auth0 := instance.auth0.AuthSignIn(username, password)
	response := map[string]interface{}{
		"access_token":  auth0.AccessToken,
		"refresh_token": auth0.RefreshToken,
		"auth_id":       auth0.ItemId,
		"error":         auth0.Error,
	}
	if len(auth0.Error) > 0 {
		switch auth0.Error {
		case "not_confirmed":
			// send back confirm token
			confirmToken, err := instance.database.GetUserConfirmToken(field, username)
			if nil != err {
				return nil, err
			}
			response["confirm_token"] = confirmToken
			return response, UserNotConfirmedError
		default:
			return nil, UserNotFoundError
		}
	}
	return response, nil
}

func (instance *AppWebserverAuthentication) writeHtmlError(ctx *fiber.Ctx, err error) error {
	html, e := instance.postman.GetHtmlTemplate(tplError)
	if nil != e {
		return WriteHtml(ctx, err.Error())
	}
	return WriteHtml(ctx, fmt.Sprintf(html, err.Error()))
}

// event emitted from internal ticker every 1 hour.
// check here everything about
func (instance *AppWebserverAuthentication) onAuthTick(_ *gg_ticker.Ticker) {
	if nil != instance && instance.settings.Enabled {
		if instance.settings.AccountNotConfirmedExpireInDays > 0 {
			// remove not confirmed accounts
			instance.authRemoveExpiredAccounts(instance.settings.AccountNotConfirmedExpireInDays)
		}
	}
}

func (instance *AppWebserverAuthentication) authRemoveExpiredAccounts(days int) {
	users := instance.database.GetUserNotVerified(days)
	for _, user := range users {
		key := gg.Reflect.GetString(user, "_key")
		_, _ = instance.removeUser(key)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func getRouting(source *httpoo_types.WebServerSettingsAuthenticationRouting) *httpoo_types.WebServerSettingsAuthenticationRouting {
	result := httpoo_types.NewWebServerSettingsAuthenticationRouting()
	if nil != source {
		// sign-in
		if nil != source.AuthSignIn {
			if len(source.AuthSignIn.Method) > 0 {
				result.AuthSignIn.Method = source.AuthSignIn.Method
			}
			if len(source.AuthSignIn.Endpoint) > 0 {
				result.AuthSignIn.Endpoint = source.AuthSignIn.Endpoint
			}
			if len(source.AuthSignIn.ParamsRequired) > 0 {
				result.AuthSignIn.ParamsRequired = source.AuthSignIn.ParamsRequired
			}
			if len(source.AuthSignIn.Params) > 0 {
				result.AuthSignIn.Params = source.AuthSignIn.Params
			}
		}

		// sign-up
		if nil != source.AuthSignUp {
			if len(source.AuthSignUp.Method) > 0 {
				result.AuthSignUp.Method = source.AuthSignUp.Method
			}
			if len(source.AuthSignUp.Endpoint) > 0 {
				result.AuthSignUp.Endpoint = source.AuthSignUp.Endpoint
			}
			if len(source.AuthSignUp.ParamsRequired) > 0 {
				result.AuthSignUp.ParamsRequired = source.AuthSignUp.ParamsRequired
			}
			if len(source.AuthSignUp.Params) > 0 {
				result.AuthSignUp.Params = source.AuthSignUp.Params
			}
		}

		// verify
		if nil != source.AuthVerify {
			if len(source.AuthVerify.Method) > 0 {
				result.AuthVerify.Method = source.AuthVerify.Method
			}
			if len(source.AuthVerify.Endpoint) > 0 {
				result.AuthVerify.Endpoint = source.AuthVerify.Endpoint
			}
			if len(source.AuthVerify.ParamsRequired) > 0 {
				result.AuthVerify.ParamsRequired = source.AuthVerify.ParamsRequired
			}
			if len(source.AuthVerify.Params) > 0 {
				result.AuthVerify.Params = source.AuthVerify.Params
			}
		}

		// forgot-password
		if nil != source.AuthForgotPassword {
			if len(source.AuthForgotPassword.Method) > 0 {
				result.AuthForgotPassword.Method = source.AuthForgotPassword.Method
			}
			if len(source.AuthForgotPassword.Endpoint) > 0 {
				result.AuthForgotPassword.Endpoint = source.AuthForgotPassword.Endpoint
			}
			if len(source.AuthForgotPassword.ParamsRequired) > 0 {
				result.AuthForgotPassword.ParamsRequired = source.AuthForgotPassword.ParamsRequired
			}
			if len(source.AuthForgotPassword.Params) > 0 {
				result.AuthForgotPassword.Params = source.AuthForgotPassword.Params
			}
		}

		// change-password
		if nil != source.AuthChangePassword {
			if len(source.AuthChangePassword.Method) > 0 {
				result.AuthChangePassword.Method = source.AuthChangePassword.Method
			}
			if len(source.AuthChangePassword.Endpoint) > 0 {
				result.AuthChangePassword.Endpoint = source.AuthChangePassword.Endpoint
			}
			if len(source.AuthChangePassword.ParamsRequired) > 0 {
				result.AuthChangePassword.ParamsRequired = source.AuthChangePassword.ParamsRequired
			}
			if len(source.AuthChangePassword.Params) > 0 {
				result.AuthChangePassword.Params = source.AuthChangePassword.Params
			}
		}

		// reset-password
		if nil != source.AuthResetPassword {
			if len(source.AuthResetPassword.Method) > 0 {
				result.AuthResetPassword.Method = source.AuthResetPassword.Method
			}
			if len(source.AuthResetPassword.Endpoint) > 0 {
				result.AuthResetPassword.Endpoint = source.AuthResetPassword.Endpoint
			}
			if len(source.AuthResetPassword.ParamsRequired) > 0 {
				result.AuthResetPassword.ParamsRequired = source.AuthResetPassword.ParamsRequired
			}
			if len(source.AuthResetPassword.Params) > 0 {
				result.AuthResetPassword.Params = source.AuthResetPassword.Params
			}
		}

		// remove
		if nil != source.AuthRemove {
			if len(source.AuthRemove.Method) > 0 {
				result.AuthRemove.Method = source.AuthRemove.Method
			}
			if len(source.AuthRemove.Endpoint) > 0 {
				result.AuthRemove.Endpoint = source.AuthRemove.Endpoint
			}
			if len(source.AuthRemove.ParamsRequired) > 0 {
				result.AuthRemove.ParamsRequired = source.AuthRemove.ParamsRequired
			}
			if len(source.AuthRemove.Params) > 0 {
				result.AuthRemove.Params = source.AuthRemove.Params
			}
		}

		// token validate
		if nil != source.AuthValidateToken {
			if len(source.AuthValidateToken.Method) > 0 {
				result.AuthValidateToken.Method = source.AuthValidateToken.Method
			}
			if len(source.AuthValidateToken.Endpoint) > 0 {
				result.AuthValidateToken.Endpoint = source.AuthValidateToken.Endpoint
			}
			if len(source.AuthValidateToken.ParamsRequired) > 0 {
				result.AuthValidateToken.ParamsRequired = source.AuthValidateToken.ParamsRequired
			}
			if len(source.AuthValidateToken.Params) > 0 {
				result.AuthValidateToken.Params = source.AuthValidateToken.Params
			}
		}

		// token refresh
		if nil != source.AuthRefreshToken {
			if len(source.AuthRefreshToken.Method) > 0 {
				result.AuthRefreshToken.Method = source.AuthRefreshToken.Method
			}
			if len(source.AuthRefreshToken.Endpoint) > 0 {
				result.AuthRefreshToken.Endpoint = source.AuthRefreshToken.Endpoint
			}
			if len(source.AuthRefreshToken.ParamsRequired) > 0 {
				result.AuthRefreshToken.ParamsRequired = source.AuthRefreshToken.ParamsRequired
			}
			if len(source.AuthRefreshToken.Params) > 0 {
				result.AuthRefreshToken.Params = source.AuthRefreshToken.Params
			}
		}

		// re-send email
		if nil != source.AuthResendActivationEmail {
			if len(source.AuthResendActivationEmail.Method) > 0 {
				result.AuthResendActivationEmail.Method = source.AuthResendActivationEmail.Method
			}
			if len(source.AuthResendActivationEmail.Endpoint) > 0 {
				result.AuthResendActivationEmail.Endpoint = source.AuthResendActivationEmail.Endpoint
			}
			if len(source.AuthResendActivationEmail.ParamsRequired) > 0 {
				result.AuthResendActivationEmail.ParamsRequired = source.AuthResendActivationEmail.ParamsRequired
			}
			if len(source.AuthResendActivationEmail.Params) > 0 {
				result.AuthResendActivationEmail.Params = source.AuthResendActivationEmail.Params
			}
		}

		// import
		if nil != source.AuthImport {
			if len(source.AuthImport.Method) > 0 {
				result.AuthImport.Method = source.AuthImport.Method
			}
			if len(source.AuthImport.Endpoint) > 0 {
				result.AuthImport.Endpoint = source.AuthImport.Endpoint
			}
			if len(source.AuthImport.ParamsRequired) > 0 {
				result.AuthImport.ParamsRequired = source.AuthImport.ParamsRequired
			}
			if len(source.AuthImport.Params) > 0 {
				result.AuthImport.Params = source.AuthImport.Params
			}
		}
	}
	return result
}

// validate security.app_token if enabled.
func validate(ctx *fiber.Ctx, token string) bool {
	if len(token) > 0 {
		// access token-token
		tokens := GetAuthTokens(ctx)
		for _, t := range tokens {
			if token == t {
				return true
			}
		}
		_ = WriteResponse(ctx, nil, httpoo_commons.HttpUnauthorizedError)
		return false
	}
	return true
}

func getLink(link, confirmToken string) string {
	fieldName := gg.Arrays.GetAt(strings.Split(link, ":"), 1, "")
	return "." + strings.ReplaceAll(link, fmt.Sprintf(":%v", fieldName), url.QueryEscape(confirmToken))
}

func getChannels(params map[string]interface{}) []string {
	response := getSysParamsValues(params, ".channels")
	if len(response) == 0 {
		response = append(response, channelEMAIL)
	}
	return response
}

func getAddresses(params map[string]interface{}, defValue string) []string {
	response := getSysParamsValues(params, ".addresses")
	if len(response) == 0 {
		response = append(response, defValue)
	}
	return response
}

func getSysParamsValues(params map[string]interface{}, name string) []string {
	response := make([]string, 0)
	if len(params) > 0 {
		for k, v := range params {
			if k == name {
				response = append(response, fmt.Sprintf("%v", v))
			}
		}
	}
	return response
}

func removeFiles(files []string, dir string) {
	for _, name := range files {
		filename := gg.Paths.Concat(dir, name)
		if b, _ := gg.Paths.Exists(filename); b {
			_ = gg.IO.Remove(filename)
		}
	}
	files, _ = gg.Paths.ListFiles(dir, "*.*")
	if len(files) == 0 {
		_ = gg.IO.RemoveAll(dir)
		_ = gg.Paths.Mkdir(gg.Paths.ConcatDir(dir))
	}
}
