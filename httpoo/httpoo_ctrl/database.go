package httpoo_ctrl

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0"
	"errors"
	"strings"
	"time"
)

const (
	CollectionUsers = "users"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserverAuthenticationDatabase struct {
	config      *gg_auth0.Auth0ConfigStorage
	collection  string
	initialized bool
	_database   gg_dbal_drivers.IDatabase
	err         error
}

func NewAppWebserverAuthenticationDatabase(config *gg_auth0.Auth0ConfigStorage, collection string) *AppWebserverAuthenticationDatabase {
	instance := new(AppWebserverAuthenticationDatabase)
	instance.config = config
	instance.collection = collection
	if len(instance.collection) == 0 {
		instance.collection = CollectionUsers
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationDatabase) UserEmailExists(fieldName, fieldValue string) (bool, error) {
	if nil != instance.err {
		return false, instance.err
	}
	data, err := instance.database().Find(instance.collection, fieldName, fieldValue)
	if nil != err {
		return false, err
	}
	array := gg.Convert.ToArray(data)
	if len(array) > 0 {
		return true, nil
	}
	return false, nil
}

func (instance *AppWebserverAuthenticationDatabase) CreateNewUser(authResponse *gg_auth0.Auth0Response, payload map[string]interface{}) (map[string]interface{}, error) {
	if nil != instance.err {
		return nil, instance.err
	}

	now := time.Now().Unix()
	user := payload
	user["auth_id"] = authResponse.ItemId
	user["confirm_token"] = authResponse.ConfirmToken
	user["member_since"] = now
	user["last_password_change"] = now
	user["account_verified"] = false
	user["role"] = 0

	return instance.database().Upsert(instance.collection, user)
}

func (instance *AppWebserverAuthenticationDatabase) UpdateUser(data map[string]interface{}, passwordChanged bool) (map[string]interface{}, error) {
	if nil != instance.err {
		return nil, instance.err
	}
	user := make(map[string]interface{})
	if passwordChanged {
		user["last_password_change"] = time.Now().Unix()
	}
	// copy user data, except protected one
	for k, v := range data {
		switch k {
		case "auth_id":
			if passwordChanged {
				user[k] = v
			}
		case "password":
			// not assigned for security leak
		default:
			user[k] = v
		}
	}
	return instance.database().Upsert(instance.collection, user)
}

func (instance *AppWebserverAuthenticationDatabase) RemoveUser(key string) (map[string]interface{}, error) {
	if nil != instance.err {
		return nil, instance.err
	}
	user, err := instance.database().Get(instance.collection, key)
	if nil != err {
		return nil, err
	}
	err = instance.database().Remove(instance.collection, key)
	return user, err
}

func (instance *AppWebserverAuthenticationDatabase) GetUser(field string, value interface{}) (map[string]interface{}, error) {
	data, err := instance.database().Find(instance.collection, field, value)
	if nil != err {
		return nil, err
	}
	array := gg.Convert.ToArray(data)
	if len(array) > 0 {
		user := array[0]
		if v, b := user.(map[string]interface{}); b {
			return v, nil
		}
	}
	return nil, nil
}

func (instance *AppWebserverAuthenticationDatabase) GetUserConfirmToken(field string, value interface{}) (string, error) {
	data, err := instance.database().Find(instance.collection, field, value)
	if nil != err {
		return "", err
	}
	array := gg.Convert.ToArray(data)
	if len(array) > 0 {
		user := array[0]
		return gg.Reflect.GetString(user, "confirm_token"), nil
	}
	return "", nil
}

func (instance *AppWebserverAuthenticationDatabase) ConfirmUser(authId string) (map[string]interface{}, error) {
	user, err := instance.GetUser("auth_id", authId)
	if nil != err {
		return nil, err
	}
	user["account_verified"] = true
	user["confirm_token"] = ""
	return instance.database().Upsert(instance.collection, user)
}

func (instance *AppWebserverAuthenticationDatabase) GetUserNotVerified(days int) []map[string]interface{} {
	response := make([]map[string]interface{}, 0)
	if nil == instance.err && days > 0 {
		_ = instance.database().ForEach(instance.collection, func(m map[string]interface{}) bool {
			accountVerified := gg.Reflect.GetBool(m, "account_verified")
			if !accountVerified {
				memberSince := int64(gg.Reflect.GetInt(m, "member_since"))
				memberSinceDays := time.Now().Sub(time.Unix(memberSince, 0)).Hours() / 24
				if int(memberSinceDays) > days {
					response = append(response, m)
				}
			}
			return false
		})
	}
	return response
}

func (instance *AppWebserverAuthenticationDatabase) ParseUserFromFile(file string) ([]map[string]interface{}, error) {
	switch strings.ToLower(gg.Paths.ExtensionName(file)) {
	case "json":
		return instance.parseJSON(file)
	case "csv", "txt":
		return instance.parseTEXT(file)
	}
	return nil, errors.New("filetype_not_supported")
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserverAuthenticationDatabase) database() gg_dbal_drivers.IDatabase {
	instance.err = nil
	if nil == instance._database {
		instance._database, instance.err = gg_dbal_drivers.NewDatabase(instance.config.Driver, instance.config.Dsn)
		if nil != instance.err {
			instance._database = nil
		} else {
			instance.init(instance._database)
		}
	}
	return instance._database
}

func (instance *AppWebserverAuthenticationDatabase) init(database gg_dbal_drivers.IDatabase) (err error) {
	if nil != database && !instance.initialized {
		driver := database.DriverName()
		switch driver {
		case gg_dbal_drivers.NameArango:
			if arango, b := database.(*gg_dbal_drivers.DriverArango); b {
				_, _ = arango.EnsureCollection(instance.collection)

				instance.initialized = true
			}
		default:
			// not supported
		}

	}
	return
}

func (instance *AppWebserverAuthenticationDatabase) parseTEXT(file string) ([]map[string]interface{}, error) {
	response := make([]map[string]interface{}, 0)
	text, err := gg.IO.ReadTextFromFile(file)
	if nil != err {
		return nil, err
	}
	if len(text) > 0 {
		options := gg.CSV.NewCsvOptionsDefaults()
		data, err := gg.CSV.ReadAll(text, options)
		if nil != err {
			return nil, err
		}
		for _, item := range data {
			response = append(response, gg.Convert.ToMap(item))
		}
	}
	return response, nil
}

func (instance *AppWebserverAuthenticationDatabase) parseJSON(file string) ([]map[string]interface{}, error) {
	response := make([]map[string]interface{}, 0)
	bytes, err := gg.IO.ReadBytesFromFile(file)
	if nil != err {
		return nil, err
	}
	if len(bytes) > 0 {
		if string(bytes[0]) == "{" {
			var m map[string]interface{}
			err = gg.JSON.Read(bytes, &m)
			if nil != err {
				return nil, err
			}
			response = append(response, m)
		} else {
			var a []map[string]interface{}
			err = gg.JSON.Read(bytes, &a)
			if nil != err {
				return nil, err
			}
			response = append(response, a...)
		}
	}
	return response, nil
}
