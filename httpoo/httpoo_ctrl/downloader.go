package httpoo_ctrl

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
)

/**
Download templates from GitHub or Bitbucket
*/

const (
	repoRoot = "https://bitbucket.org/digi-sense/gg-module-httpoo/raw/master/"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func DownloadTemplates(targetDir string) ([]string, []error) {
	// download
	session := gg.IO.NewDownloadSession(templateActions(targetDir))
	return session.DownloadAll(false)
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func templateActions(dirTemplates string) []*gg_utils.DownloaderAction {
	response := make([]*gg_utils.DownloaderAction, 0)

	// email
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/password-reset.html.htm", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/password-reset.text.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/password-reset.subject.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/verify.html.htm", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/verify.text.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/verify.subject.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/welcome.html.htm", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/welcome.text.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/email/welcome.subject.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "email")))

	// sms
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/sms/verify.text.txt", "",
		gg.Paths.ConcatDir(dirTemplates, "sms")))

	// html
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/html/error.html", "",
		gg.Paths.ConcatDir(dirTemplates, "html")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/html/password-reset.html", "",
		gg.Paths.ConcatDir(dirTemplates, "html")))
	response = append(response, gg.IO.NewDownloaderAction("",
		repoRoot+"httpoo_templates/html/verified.html", "",
		gg.Paths.ConcatDir(dirTemplates, "html")))

	return response
}
