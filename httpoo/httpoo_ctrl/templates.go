package httpoo_ctrl

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"github.com/cbroglie/mustache"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AuthenticationTemplates struct {
	dirWork      string
	dirTemplates string
}

func NewAuthenticationTemplates(dirWork, dirTemplates string) *AuthenticationTemplates {
	instance := new(AuthenticationTemplates)
	instance.dirWork = dirWork
	instance.dirTemplates = dirTemplates

	// initialize directories
	if len(instance.dirTemplates) == 0 {
		instance.dirTemplates = "./templates"
	}
	instance.dirTemplates = gg.Paths.Absolutize(instance.dirTemplates, instance.dirWork)
	_ = gg.Paths.Mkdir(instance.dirTemplates + gg_utils.OS_PATH_SEPARATOR)

	// creates sub dirs
	_ = gg.Paths.Mkdir(gg.Paths.Concat(instance.dirTemplates, "email") + gg_utils.OS_PATH_SEPARATOR)
	_ = gg.Paths.Mkdir(gg.Paths.Concat(instance.dirTemplates, "sms") + gg_utils.OS_PATH_SEPARATOR)
	_ = gg.Paths.Mkdir(gg.Paths.Concat(instance.dirTemplates, "html") + gg_utils.OS_PATH_SEPARATOR)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AuthenticationTemplates) Check() ([]string, []error) {
	// download templates
	return DownloadTemplates(instance.dirTemplates)
}

// Render sample: Render("email", "verify.html", {"username":"Mario"})
func (instance *AuthenticationTemplates) Render(dir, name string, data map[string]interface{}) (string, error) {
	content, err := instance.Get(dir, name)
	if nil != err {
		return "", err
	}
	return mustache.Render(content, data)
}

// Get sample: Get("email", "verify.html")
func (instance *AuthenticationTemplates) Get(dir, name string) (string, error) {
	filename := gg.Paths.Concat(instance.dirTemplates, dir, name)
	return gg.IO.ReadTextFromFile(filename)
}
