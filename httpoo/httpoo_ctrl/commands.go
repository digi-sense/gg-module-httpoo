package httpoo_ctrl

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_dbal/gg_dbal_drivers"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_types"
	"strings"
)

type AppCommands struct {
	defConfig   *httpoo_types.Database
	initialized bool
}

func NewAppCommands(defConfig *httpoo_types.Database) *AppCommands {
	instance := new(AppCommands)
	instance.defConfig = defConfig

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppCommands) Execute(filename string, params interface{}, dbConfig *httpoo_types.Database) (interface{}, error) {
	ext := strings.ToLower(gg.Paths.ExtensionName(filename))
	switch ext {
	case "sh":
		args := gg.Convert.ToArrayOfString(params)
		return gg.Exec.RunOutput(filename, args...)
	case "aql":
		return instance.executeAQL(filename, params, dbConfig)
	default:
		return nil, httpoo_commons.CommandUnsupported
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppCommands) database(dbConfig *httpoo_types.Database) (gg_dbal_drivers.IDatabase, error){
	if nil == dbConfig || len(dbConfig.Driver)==0{
		dbConfig = instance.defConfig
	}
	return gg_dbal_drivers.NewDatabase(dbConfig.Driver, dbConfig.Dsn)
}

func (instance *AppCommands) executeAQL(filename string, params interface{}, dbConfig *httpoo_types.Database) (interface{}, error) {
	db, err := instance.database(dbConfig)
	if nil != err {
		return nil, err
	}
	query, err := gg.IO.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}

	mParams := db.QuerySelectParams(query, gg.Convert.ForceMap(params))
	return db.ExecNative(query, mParams)
}
