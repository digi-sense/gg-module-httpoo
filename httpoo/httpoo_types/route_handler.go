package httpoo_types

import (
	"github.com/gofiber/fiber/v2"
)

type IRouteHandler interface {
	Method() string
	Endpoint() *EndPoint
	Handle(ctx *fiber.Ctx) error
}
