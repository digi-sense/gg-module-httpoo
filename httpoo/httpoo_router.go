package httpoo

import (
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_ctrl"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_types"
	"github.com/gofiber/fiber/v2"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type HandlerCallback func(ctx *fiber.Ctx, endpoint *RouteHandler) error

type RouteHandler struct {
	method        string
	endpoint      *httpoo_types.EndPoint
	authorization *httpoo_types.Authorization
	database      *httpoo_types.Database
	auth          *httpoo_ctrl.AppSecure
	callback      HandlerCallback
}

func NewRouteHandler(auth *httpoo_ctrl.AppSecure, config *httpoo_types.WebServerSettingsRoute, callback HandlerCallback) *RouteHandler {
	instance := new(RouteHandler)
	instance.auth = auth
	instance.method = strings.ToLower(config.Method)
	instance.endpoint = httpoo_types.EndPointParse(config.Endpoint)
	instance.authorization = config.Authorization
	instance.database = config.Database
	instance.callback = callback

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *RouteHandler) Method() string {
	if nil != instance {
		return instance.method
	}
	return ""
}

func (instance *RouteHandler) Endpoint() *httpoo_types.EndPoint {
	if nil != instance {
		return instance.endpoint
	}
	return nil
}

func (instance *RouteHandler) Handle(ctx *fiber.Ctx) error {
	if nil != instance {
		err := instance.run(ctx)
		if instance.isMiddleware() {
			return ctx.Next()
		}
		return err
	}

	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *RouteHandler) isReady() bool {
	if nil != instance && nil != instance.callback {
		return true
	}
	return false
}

func (instance *RouteHandler) isMiddleware() bool {
	if nil != instance {
		return instance.method == "middleware"
	}
	return false
}

func (instance *RouteHandler) run(ctx *fiber.Ctx) error {
	if nil != instance && instance.isReady() && nil != instance.callback {
		if AuthenticateRequest(ctx, instance.auth.Auth0(), instance.authorization, false) {
			return instance.callback(ctx, instance)
		} else {
			return httpoo_commons.HttpUnauthorizedError
		}
	}
	return httpoo_commons.EndpointNotReadyError
}
