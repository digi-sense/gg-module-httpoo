package httpoo

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_ctrl"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_types"
	"path/filepath"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		Interfaces
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
//		A p p l i c a t i o n
// ---------------------------------------------------------------------------------------------------------------------

type HTTPoo struct {
	mode    string
	root    string
	dirWork string

	stopChan      chan bool
	secureManager *httpoo_ctrl.AppSecure
	stopMonitor   *stopMonitor
	events        *gg_events.Emitter

	webserver *AppWebserver
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HTTPoo) Start() (err error) {
	instance.stopChan = make(chan bool, 1)
	if nil != instance.stopMonitor {
		instance.stopMonitor.Start()
	}
	if nil != instance.webserver {
		err = instance.webserver.Start()
	}
	return
}

// Stop Try to close gracefully
func (instance *HTTPoo) Stop() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}
	if nil != instance.webserver {
		go func() {
			instance.webserver.Stop()
		}()
	}
	time.Sleep(3 * time.Second)
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}
	return
}

// Exit application
func (instance *HTTPoo) Exit() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}
	if nil != instance.webserver {
		instance.webserver.Exit()
	}
	return
}

func (instance *HTTPoo) Join() {
	if nil != instance.stopChan {
		<-instance.stopChan
	}
}

func (instance *HTTPoo) LocalUrl() string {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.LocalUrl()
	}
	return ""
}

func (instance *HTTPoo) Handle(method, endpoint string, auth *httpoo_types.Authorization, handler HandlerCallback) {
	if nil != instance && nil != instance.webserver {
		if len(method) == 0 {
			method = "all"
		}
		instance.webserver.Handle(method, endpoint, auth, handler)
	}
}

func (instance *HTTPoo) HandleRoute(route *httpoo_types.WebServerSettingsRoute, handler HandlerCallback) {
	if nil != instance && nil != instance.webserver && nil != route {
		instance.webserver.HandleRoute(route, handler)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HTTPoo) doStop(event *gg_events.Event) {
	_ = instance.Exit()
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func LaunchHTTPoo(mode, cmdStop string, args ...interface{}) (instance *HTTPoo, err error) {
	instance = new(HTTPoo)
	instance.mode = mode

	// paths
	instance.dirWork = gg.Paths.GetWorkspace(httpoo_commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)

	instance.events = gg.Events.NewEmitter("httpoo")
	instance.stopMonitor = newStopMonitor([]string{instance.root, instance.dirWork}, cmdStop, instance.events)
	instance.events.On(httpoo_commons.EventOnDoStop, instance.doStop)

	logger := gg.Arrays.GetAt(args, 0, nil)
	instance.webserver = NewAppWebserver(mode, logger)

	return
}
