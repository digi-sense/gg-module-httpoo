package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"log"
	"testing"
)

func TestUI(t *testing.T) {
	// set working path
	gg.Paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(gg.Paths.Absolute("./"))

	// create server
	server, err := httpoo.LaunchHTTPoo("debug", "stop")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = server.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// get server URL
	url := server.LocalUrl()
	log.Println("STARTED:", url)

	// open browser
	go func() {
		// OPEN DEFAULT BROWSER NAVIGATING http://localhost:9090/
		_ = gg.Exec.Open(url)
	}()

	// wait for exit
	server.Join()
	log.Println("BYE BYE!")
}


// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

