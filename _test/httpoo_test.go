package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"github.com/gofiber/fiber/v2"
	"log"
	"testing"
	"time"
)

func TestHandler(t *testing.T) {
	// set working path
	gg.Paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(gg.Paths.Absolute("./"))

	// create server
	server, err := httpoo.LaunchHTTPoo("debug", "stop")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// add custom API handler
	server.Handle("get", "custom/endpoint", nil, handleGet)
	err = server.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// get server URL
	url := server.LocalUrl()
	log.Println("STARTED:", url)

	// open browser
	go func() {
		_ = gg.Exec.Open(url + "custom/endpoint")
	}()

	// wait 3 seconds and exit
	go func() {
		time.Sleep(3 * time.Second)
		log.Println("STOPPING...")
		_ = server.Stop()
	}()

	// wait for exit
	server.Join()
	log.Println("BYE BYE!")
}

func TestStopWithFile(t *testing.T) {
	// set working path
	gg.Paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(gg.Paths.Absolute("./"))

	// create server
	server, err := httpoo.LaunchHTTPoo("debug", "stop")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// add custom API handler
	server.Handle("get", "custom/endpoint", nil, handleGet)
	err = server.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// get server URL
	url := server.LocalUrl()
	log.Println("STARTED:", url)

	// open browser
	go func() {
		_ = gg.Exec.Open(url + "custom/endpoint")
	}()

	// wait 3 seconds and exit
	go func() {
		time.Sleep(3 * time.Second)
		log.Println("STOPPING...")

		_, _ = gg.IO.WriteTextToFile("", "./stop")
	}()

	// wait for exit
	server.Join()
	log.Println("BYE BYE!")
}

func TestCommand(t *testing.T) {

	commands := []string{
		"api/v1/commands/command1.sh?token=123:abc",
		"api/v1/commands/command.aql?token=123:abc&email=angelo.geminiani@gmail.com",
	}
	// set working path
	gg.Paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(gg.Paths.Absolute("./"))

	// create server
	server, err := httpoo.LaunchHTTPoo("debug", "stop")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = server.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// get server URL
	url := server.LocalUrl()
	log.Println("SERVING AT:", url)

	// open browser launching a command
	go func() {
		for _, command:=range commands{
			_ = gg.Exec.Open(url + command)
		}
	}()

	// wait for exit
	server.Join()
	log.Println("BYE BYE!")
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func handleGet(ctx *fiber.Ctx, handler *httpoo.RouteHandler) error {
	log.Println("GET: " + ctx.OriginalURL())
	return httpoo.WriteHtml(ctx, "<body><strong>DONE!</strong></body>")
}
