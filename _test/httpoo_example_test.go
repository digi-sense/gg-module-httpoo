package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo"
	"bitbucket.org/digi-sense/gg-module-httpoo/httpoo/httpoo_commons"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"log"
	"testing"
	"time"
)

func TestCustomHandler(t *testing.T) {
	// set working path
	gg.Paths.GetWorkspace(httpoo_commons.WpDirWork).SetPath(gg.Paths.Absolute("./"))

	// create server
	server, err := httpoo.LaunchHTTPoo("debug", "stop")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// add custom API handler
	server.Handle("get", "custom/endpoint", nil, customHandler)
	err = server.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// get server URL
	url := server.LocalUrl()
	log.Println("STARTED:", url)

	// open browser
	go func() {
		// OPEN DEFAULT BROWSER NAVIGATING http://localhost:9090/custom/endpoint
		_ = gg.Exec.Open(url + "custom/endpoint")
	}()

	// wait 3 seconds and exit
	go func() {
		time.Sleep(3 * time.Second)
		log.Println("STOPPING...")
		_ = server.Stop()
	}()

	// wait for exit
	server.Join()
	log.Println("BYE BYE!")
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func customHandler(ctx *fiber.Ctx, endpoint *httpoo.RouteHandler) error {
	log.Println("GET: " + ctx.OriginalURL())
	text := fmt.Sprintf("<body> %s <strong>DONE!</strong></body>", time.Now())
	return httpoo.WriteHtml(ctx, text)
}
